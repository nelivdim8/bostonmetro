package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Station {

	private final String name;
	private final int number;
	private List<String> lines;
	

	public Station(String name, int number) {
		super();
		this.name = name;
		this.number = number;
	}

	public Station(String name, int number, LinkedList<String> lines) {

		this(name,number);
		this.lines = lines;

	}

	public Station() {
		this("", 0, new LinkedList<>());
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public List<String> getLines() {
		return lines;
	}


	@Override
	public String toString() {
		return "Station [name=" + name + ", number=" + number + "]";
	}

}