package model;

public class Track {
	
	private final String line;
	private final Station from;
	private final Station to;
	
	

	public Track(Station from,Station to,String line) {
		super();
		this.from=from;
		this.to=to;
		this.line=line;
		
	}



	public String getLine() {
		return line;
	}



	public Station getFrom() {
		return from;
	}



	public Station getTo() {
		return to;
	}
  


	@Override
	public String toString() {
		return "Track [line=" + line + ", from=" + from.toString() + ", to=" + to.toString() + "]";
	}



}
