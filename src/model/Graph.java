package model;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph {

	private Map<Station, List<Station>> neighbors = new HashMap<Station, List<Station>>();
	private List<Integer> visited = new LinkedList<>();

	public boolean addVertex(Station vertex) {

		if (!neighbors.containsKey(vertex)) {
			neighbors.put(vertex, new ArrayList<>());
			return true;
		}
		return false;
	}

	public boolean containsVertex(Station v) {
		return neighbors.containsKey(v);
	}
	public Station getVertexByNumber(Integer number) {
		Station result=null;
	Set<Station> keys=neighbors.keySet();
	for (Station station : keys) {
		if(station.getNumber()==number) {
			result=station;
		}
	}
	return result;
	
		
	}

	public boolean addEdge(Station from, Station to) {
		
		if(!this.containsVertex(from)) {
			this.addVertex(from);
		}
		if(!this.containsVertex(to)) {
			this.addVertex(to);
		}
		if(!hasEdge(from,to)) {
		neighbors.get(from).add(to);
		neighbors.get(to).add(from);
		return true;
		}else {
		return false;
		}

	}

	public boolean removeEdge(Station from, Station to) {
		
	
		
			if (hasEdge(from, to)) {
				neighbors.get(from).remove(to);
				neighbors.get(to).remove(from);
				return true;
			}else {
				return false;
			}
		
			
		
	}

	public boolean hasEdge(Station from, Station to) {
		if(neighbors.containsKey(from)) {
		for (Station e : neighbors.get(from)) {
			if (e.equals(to)) {
				
				return true;
			}

		}
		}
		return false;

	}

	public List<Integer> neighbours(Integer number) {
	Station vertex=getVertexByNumber(number);
		
		List<Integer> list = new ArrayList<>();
		for (Station v : neighbors.get(vertex)) {
			list.add(v.getNumber());

		}

		return list;

	}
	


	// key
	public List<Integer> shortestPath(Integer src,Integer dest) {

		List<List<Integer>> paths = bfsPaths(src, dest);

		return paths.get(0);
	}

	public List<List<Integer>> bfsPaths(Integer src, Integer dest) {
		List<Integer> result = bfs(src, dest);

		List<List<Integer>> results = new LinkedList<List<Integer>>();
		int counter = 0;
		int start = 0;

		for (int j = 1; j < result.size(); j++) {
			if (result.get(j) == src) {
				results.add(counter, result.subList(start, j));
				counter++;
				start = j;

			}

		}
		results.add(counter, result.subList(start, result.size()));
		Collections.sort(results, new ListComparator<Integer>());
		return results;
	}

	public LinkedList<Integer> bfs(Integer src, Integer dest) {
		LinkedList<Integer> result = new LinkedList<>();
		LinkedList<Integer> shortest = new LinkedList<>();
		if (src.equals(dest)) {
			shortest.add(src);

		} else {

			bfsHelper(src, result, dest, shortest);
		}
		return shortest;

	}

	private boolean visited(Integer vertex) {
		if (visited.contains(vertex)) {
			return true;
		}
		return false;
	}

	private void bfsHelper(Integer src, LinkedList<Integer> result,Integer dest, LinkedList<Integer> shortest) {

		visited.add(src);
		result.add(src);
		if (src.equals(dest)) {
			for (Iterator<Integer> iterator = result.iterator(); iterator.hasNext();) {
				Integer node1 = iterator.next();
				shortest.add(node1);
			}

		} else {
			List<Integer> neighbors = neighbours(src);

			for (int j = 0; j <neighbors.size(); j++) {

				Integer n = neighbors.get(j);
				if (!visited(n)) {

					bfsHelper(n, result, dest, shortest);

				}

			}

		}

		visited.remove(src);
		result.remove(result.size() - 1);

	}

}
