package model;

import java.util.Comparator;
import java.util.List;

public class ListComparator<T> implements Comparator<List<T>>{

	@Override
	public int compare(List<T> o1, List<T> o2) {
		if (o1.size() < o2.size()) {
			return -1;
		} else if (o1.size() > o2.size()) {
			return 1;
		} else
			return 0;
	}

}


