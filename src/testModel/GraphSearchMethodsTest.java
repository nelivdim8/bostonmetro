package testModel;
import model.*;


import static org.junit.Assert.*;


import java.util.List;

import org.junit.After;
import org.junit.Before;

import org.junit.Test;


public class GraphSearchMethodsTest {
    Station s1;
	Station s2;
	Station s3;
	Station s4;
	
	
	Graph g;

	@Before
	public void setData() {
		 g = new Graph();
		 s1 = new Station("Station1", 1);
		 s2 = new Station("Station2", 2);
		 s3 = new Station("Station3", 3);
		 s4 = new Station("Station4", 4);
			g.addEdge(s1, s2);
			g.addEdge(s1, s3);
			g.addEdge(s1, s4);
			g.addEdge(s2, s3);
	}
	


	@Test
	public void testNeighbours() {
      
		Integer[] actual = g.neighbours(s1.getNumber()).toArray(new Integer[g.neighbours(s1.getNumber()).size()]);
		Integer[] expected=new Integer[] {s2.getNumber(),s3.getNumber(),s4.getNumber()};
		assertArrayEquals(expected, actual);
		

	}

	@Test
	public void testShortestPath() {
	
	Integer[] result=g.shortestPath(s1.getNumber(), s3.getNumber()).toArray(new Integer[g.shortestPath(s1.getNumber(), s3.getNumber()).size()]);
	Integer[] expected=new Integer[]{1,3};
	
	assertArrayEquals(result, expected);
	
	}

	@Test
	public void testBfsPaths() {
	   List<List<Integer>> paths=g.bfsPaths(s1.getNumber(), s3.getNumber());
	   Integer[][] result=new Integer[paths.size()][];
	   for (int i = 0; i <paths.size(); i++) {
	   Integer[] path=paths.get(i).toArray(new Integer[0]);
	   result[i]=path;

			
		}
	Integer[][] expected=new Integer[][]{{1,3},{1,2,3}};
	
	assertArrayEquals(result, expected);
	
	
	}

	@Test
	public void testBfs() {
		Integer[] result=g.bfs(s1.getNumber(), s3.getNumber()).toArray(new Integer[g.bfs(s1.getNumber(), s3.getNumber()).size()]);
		Integer[] expected=new Integer[]{1,2,3,1,3};
		assertArrayEquals(result, expected);
		Integer[] equals=g.bfs(s1.getNumber(), s1.getNumber()).toArray(new Integer[g.bfs(s1.getNumber(), s1.getNumber()).size()]);
		assertArrayEquals(equals, new Integer[] {s1.getNumber()});
		
	}

	@After
	public void clean() {
	g=null;
	s1=null;
	s2=null;
	s3=null;
	s4=null;
		
	}

}
