package testModel;
import model.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GraphVertexAndEdgeMethodsTest {
	Station s1;
	Station s2;
	Station s3;
	Station s4;

	Graph g;

	@Before
	public void setData() {
		g = new Graph();
		s1 = new Station("Station1", 1);
		s2 = new Station("Station2", 2);
		s3 = new Station("Station3", 3);
		s4 = new Station("Station3", 4);
        
		g.addVertex(s1);
		g.addVertex(s2);
		g.addVertex(s3);
		

	}

	@Test
	public void testContainsVertex() {

		assertTrue(g.containsVertex(s1));
	}

	@Test
	public void testGetVertexByNumber() {

		Station s = g.getVertexByNumber(1);
		assertEquals(s1, s);
	}

	@Test
	public void testHasEdge() {

		assertFalse(g.hasEdge(s1, s2));
		assertFalse(g.hasEdge(s4, s2));
	}

	@Test
	public void testAddEdge() {

		assertTrue(g.addEdge(s1, s2));
		assertFalse(g.addEdge(s1, s2));
		
		
	}
	

	@Test
	public void testRemoveEdge() {
		g.addEdge(s1, s2);
	    assertTrue(g.removeEdge(s1, s2));
		assertFalse(g.removeEdge(s2,s3));
	}
	
	@Test
	public void testGetNeighbours() {
		g.addEdge(s1, s2);
	    assertTrue(g.removeEdge(s1, s2));
		assertFalse(g.removeEdge(s2,s3));
	}
	@After
	public void clean() {
	g=null;
	s1=null;
	s2=null;
	s3=null;
	s4=null;
	
		
	}

}
