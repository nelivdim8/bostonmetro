package testModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import model.*;
public class TestCompareMethod {

	   
	    ListComparator comparator = new ListComparator();

	    @Test
	    public void testEqual() {
	        List<Station> list1=new ArrayList<>();
	        list1.add(new Station("Station1",1));
	        list1.add(new Station("Station2",2));
	        
	        List<Station> list2=new ArrayList<>();
	        list2.add(new Station("Station1",1));
	        list2.add(new Station("Station4",4));
	        int result = comparator.compare(list1, list2);
	        assertEquals(result, 0);
	    }

	    @Test
	    public void testGreaterThan() {
	    	 List<Station> list1=new ArrayList<>();
		        list1.add(new Station("Station1",1));
		        list1.add(new Station("Station2",2));
		        list1.add(new Station("Station5",5));
		        
		        List<Station> list2=new ArrayList<>();
		        list2.add(new Station("Station1",1));
		        list2.add(new Station("Station4",4));
		        int result = comparator.compare(list1, list2);
		
		        assertEquals(result, 1);
	    }

	    @Test
	    public void testLessThan() {
	     	 List<Station> list1=new ArrayList<>();
		        list1.add(new Station("Station1",1));
		        list1.add(new Station("Station2",2));
		        
		        List<Station> list2=new ArrayList<>();
		        list2.add(new Station("Station1",1));
		        list2.add(new Station("Station4",4));
		        list2.add(new Station("Station5",5));
		        int result = comparator.compare(list1, list2);
		
	       assertEquals(result, -1);
	    }
	}


