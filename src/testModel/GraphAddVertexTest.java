package testModel;
import model.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GraphAddVertexTest {
	
	
    Station s1;
    Graph g;

	@Before
	public void setData() {
		 g = new Graph();
		 s1 = new Station("Station1", 1);
	
		 
	}
	@Test
	public void testAddVertex() {
		assertTrue(g.addVertex(s1));
		assertFalse(g.addVertex(s1));
	}
	@After
	public void clean() {
	g=null;
	s1=null;
	
	}
}
