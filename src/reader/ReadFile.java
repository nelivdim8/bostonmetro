package reader;

import model.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class ReadFile {

	private List<LinkedList<String>> fileContent;
	private final List<Station> stations;
	private final List<Track> tracks;
	private LinkedList<String> lines;
	private Map<Integer, String> searchByNumber = new HashMap<>();
	private Map<String, Integer> searchByName;
	List<String> equalNames = new LinkedList<>();

	public ReadFile() {
		fileContent = new LinkedList<LinkedList<String>>();
		stations = new LinkedList<>();
		tracks = new LinkedList<>();
		searchByName = new TreeMap<>();

		readFromFile();
		createStations();
		findDuplicateStations();
		replaceDuplicateStations();
		createTracks();

	}

	public void readFromFile() {

		try {

			Scanner read = new Scanner(new File("bostonmetro.txt"));
			int i = 0;
			while (read.hasNextLine()) {
				String element = read.nextLine();
				List<String> contentOnLine = new LinkedList<String>(Arrays.asList(element.split("\\s+")));
				if (i < 99) {
					contentOnLine.remove(0);
				}
				i++;
				fileContent.add((LinkedList<String>) contentOnLine);
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public List<Station> createStations() {

		int id = 0;
		String name = "";
		for (int i = 0; i < fileContent.size(); i++) {
			lines = new LinkedList<>();
			for (int j = 0; j < fileContent.get(i).size(); j++) {

				if (j == 0) {
					id = Integer.parseInt(fileContent.get(i).get(j));

				}
				if (j == 1) {
					name = fileContent.get(i).get(j);
				}
				if ((j + 1) % 3 == 0) {
					String line = fileContent.get(i).get(j);
					lines.add(line);

				}
			}

			stations.add(new Station(name, id - 1, lines));

		}
		return stations;
	}

	public List<Track> createTracks() {

		int next = 0;

		String line = "";
		for (int i = 0; i < fileContent.size(); i++) {
			for (int j = 0; j < fileContent.get(i).size(); j++) {

				if ((j + 1) % 3 == 0) {
					line = fileContent.get(i).get(j);
					next = Integer.parseInt(fileContent.get(i).get(j + 2));

					if (next > 0) {
						next -= 1;
						tracks.add(new Track(stations.get(i), stations.get(next), line));

					}

				}

			}

		}
		return tracks;

	}

	public void findDuplicateStations() {

		String name = " ";
		for (int i = 0; i < stations.size(); i++) {
			if (searchByNumber.containsValue(stations.get(i).getName())) {

				equalNames.add(stations.get(i).getName());

				name = stations.get(i).getName() + " - " + stations.get(i).getLines().get(0);

				searchByNumber.put(stations.get(i).getNumber(), name);
			} else {
				searchByNumber.put(stations.get(i).getNumber(), stations.get(i).getName());
			}
		}
		
	}

	public void replaceDuplicateStations() {

		Set set = searchByNumber.entrySet();
		Iterator it = set.iterator();

		while (it.hasNext()) {
			Map.Entry m = (Map.Entry) it.next();
		
			if (equalNames.contains(m.getValue())) {
                int i=getIndex(equalNames,m.getValue().toString());
				String newValue =  equalNames.get(i)+ " - " + stations.get((Integer) m.getKey()).getLines().get(0);
				equalNames.remove(i);
				m.setValue(newValue);

			}
			searchByName.put((String) m.getValue(), (Integer) m.getKey());

		}
	

	}

	public Map<Integer, String> getSearchByNumber() {
		return searchByNumber;
	}

	public Map<String, Integer> getSearchByName() {
		return searchByName;
	}

	public int getIndex(List<String> equalsNames, String name) {
		int index = 0;
		for (int i = 0; i < equalNames.size(); i++) {
			if (equalsNames.get(i).equals(name)) {
				index = i;
				break;

			}

		}
		return index;
	}

}
