package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI {
	private JFrame frame;
	private JLabel lblStarting; 
	private JLabel lblFinal;
	private JLabel lblImage;
	private JComboBox startingBox;
	private JComboBox finalBox;
	private JButton btnFind;
	private JTextArea txaResult;
	private String[] path;

	public GUI(String[] path) {
		setStarting(path);

		showFrame();
	}

	private void showFrame() {

		frame = new JFrame("Boston Metro");

		lblStarting = new JLabel("Starting destination");
		lblStarting.setBounds(333, 20, 200, 30);
		lblStarting.setFont(new Font("Calibri", Font.BOLD, 24));
		lblFinal = new JLabel("Final destinaion");
		lblFinal.setBounds(350, 100, 200, 30);
		lblFinal.setFont(new Font("Calibri", Font.BOLD, 24));
		frame.add(lblStarting);
		frame.add(lblFinal);

		startingBox = new JComboBox(path);
		startingBox.setBounds(80, 50, 700, 40);
		startingBox.setFont(new Font("Calibri", Font.PLAIN, 24));
		frame.add(startingBox);

		finalBox = new JComboBox(path);
		finalBox.setBounds(80, 125, 700, 40);
		finalBox.setFont(new Font("Calibri", Font.PLAIN, 24));
		frame.add(finalBox);

		btnFind = new JButton();
		
		btnFind.setIcon(new ImageIcon("find.jpg")); //
		
		btnFind.setBounds(280, 200, 270,80);
		
		btnFind.setFont(new Font("Calibri", Font.BOLD, 24));
		
		frame.add(btnFind);

		txaResult = new JTextArea("Your path");
		txaResult.setBounds(80, 290, 700, 565);
		txaResult.setFont(new Font("Calibri", Font.PLAIN, 20));
		txaResult.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		frame.add(txaResult);
		

		frame.setIconImage(new ImageIcon("sign.jpg").getImage());
		
		lblImage = new JLabel("");
		lblImage.setBounds(830, 10, 870, 870);
		frame.getContentPane().add(lblImage);
		ImageIcon img2 = new ImageIcon("map.jpg");
		Image image=img2.getImage();
		Image newImage=image.getScaledInstance(lblImage.getWidth(),lblImage.getHeight(), Image.SCALE_SMOOTH);
		lblImage.setIcon(new ImageIcon(newImage));
		
		
		
		frame.setSize(1750, 950);
		frame.setLayout(null);
		frame.setResizable(true);
		frame.setVisible(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public String getStartingPosition() {
		return (startingBox.getSelectedItem()).toString();
	}

	public String getFinalPosition() {
		return (finalBox.getSelectedItem()).toString();
	}

	public int getResult() {
		return Integer.parseInt(txaResult.getText());
	}

	public void setPath(String path) {
		txaResult.setText(path);
	}

	public String[] getPath() {
		return path;
	}

	public void setStarting(String[] path) {
		this.path = path;
	}

	public void addButtonListener(ActionListener btn) {
		btnFind.addActionListener(btn);
	}
	
}
