package controller;

import model.*;
import view.*;
import reader.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Controller {
	private GUI gui;
	private Graph graph;
	private Map<String, Integer> sortedStations;

	private ReadFile reader;
	private static List<Track> tracks;

	public Controller() {
		reader = new ReadFile();
		sortedStations = reader.getSearchByName();

		tracks = reader.createTracks();
		this.graph = new Graph();
		fillGraph();
		this.gui = new GUI(sortedStations.keySet().toArray(new String[sortedStations.size()]));
		this.gui.addButtonListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String src = gui.getStartingPosition();
				String destination = gui.getFinalPosition();
				if (!src.equals(destination)) {

					List<Integer> numbers = graph.shortestPath(sortedStations.get(src),
							sortedStations.get(destination));

					gui.setPath(formatText(convertNumbersToNames(numbers)).toString());
				} else {
					gui.setPath("You are at the final destination!");
				}
			}

			public List<String> convertNumbersToNames(List<Integer> list) {

				List<String> names = new LinkedList<>();
				Map<Integer, String> reversed = reader.getSearchByNumber();
				for (Iterator<Integer> iterator = list.iterator(); iterator.hasNext();) {
					Integer i = iterator.next();
					if (reversed.containsKey(i)) {
						names.add(reversed.get(i));

					}

				}
				return names;
			}

			public StringBuilder formatText(List<String> names) {
				StringBuilder sb = new StringBuilder();

				for (int i = 0; i < names.size(); i++) {

					String string = names.get(i);
					sb.append(string);
					if (i < names.size() - 1) {
						sb.append(" => ");
					}
					if (i % 3 == 0 && i != 0) {
						sb.append("\n");
					}

				}

				return sb;
			}

		});

	}

	public void fillGraph() {
		for (Track track : tracks) {
			graph.addEdge(track.getFrom(), track.getTo());

		}
	}

	public  Map<Integer,List<Track>> example(){
	Map<Integer,List<Track>> mapping= tracks.stream().
	collect(Collectors.
	groupingBy(t->{ if(t.getFrom().
	getNumber()%2==0) return 0; 
	else return 1;}));
	return mapping;

	
			
		
	}
	 public static void main(String[] args) {
	Controller c=new Controller();
	Map<Integer,List<Track>> t=c.example();
 for (Map.Entry<Integer,List<Track>> m: t.entrySet()) {
	 System.out.print(m.getKey());
	 System.out.print(m.getValue());
	 System.out.println();
	
}
	}

}
